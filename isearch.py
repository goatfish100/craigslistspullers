from craigslist import CraigslistForSale
import json
import os
import jinja2

cl_e = CraigslistForSale(site='sfbay', category='moa', filters={'search_distance': 15, 'zip_code': 94087, 'query':'iphone+7', 'max_price': '500'})

# exclude stuff we don't want -for tickets - we don't want these terms
gsales = []


for result in cl_e.get_results(sort_by='newest'):

    gsales.append(result)
    # for item in exclude_dict:
    #     if (result['name'].lower().find(item) > -1):
    #         found_a_string = True



#
templateLoader = jinja2.FileSystemLoader( searchpath="/" )
templateEnv = jinja2.Environment( loader=templateLoader )
#
TEMPLATE_FILE = os.environ["PWD"]+'/isearch.jinja'
template = templateEnv.get_template(TEMPLATE_FILE)
#
templateVars = { "title" : "Garage Sales 5 miles from Sunnyvale",
                 "description" : "A simple inquiry of function.",
                 "favorites" : gsales
                }
#
outputText = template.render( templateVars )
#
print(outputText)
