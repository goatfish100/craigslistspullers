from craigslist import CraigslistForSale
import json
import os
import emailutil
import jinja2

cl_e = CraigslistForSale(site='phoenix', category='tia', filters={'max_price': '100', 'query':"\"parking pass\" -cardinal -dbacks -concert -diamondback -coyotes"})

# exclude stuff we don't want -for tickets - we don't want these terms
exclude_dict = ['cardinal', 'dbacks', 'diamond', 'commeric', 'diamondbacks', 'd\'backs', 'Astros', 'cardin', 'd back']
outputdict = []
outputkeys = []
inputkeys = []

for result in cl_e.get_results(sort_by='newest'):
    found_a_string = False
    for item in exclude_dict:
        if (result['name'].lower().find(item) > -1):
            found_a_string = True

    if found_a_string == False:
        # print result
        outputdict.append(result)
        outputkeys.append(result['id'])

data = []
# Load the file
if os.path.isfile(os.environ["PWD"]+'/my_file.json'):
    with open(os.environ["PWD"]+'/my_file.json', 'r') as f:
        try:
            data = json.load(f)
            for input in data:
                inputkeys.append(input['id'])
        # if the file is empty the ValueError will be thrown
        except ValueError:
            data = {}
# else write to it ....
else:
    with open(os.environ["PWD"]+'/my_file.json', 'w') as f:
        # data['new_key'] = [1, 2, 3]
        json.dump(outputdict, f)

baseemail = '''This email is sent by the Craigslist searcher
to change setting or stop recieving, email James lapointe
'''

newtickets = []
liststring = ""
for result in outputdict:
    print(result['id'])
    if result['id'] not in inputkeys:
        print("found new key")
        liststring = liststring + result['name'] + ' ' + result['url'] + '\n'
        newtickets.append(result)


if(len(newtickets)>0):
    print("in send email section")
    print(newtickets)

    templateLoader = jinja2.FileSystemLoader( searchpath="/" )
    templateEnv = jinja2.Environment( loader=templateLoader )

    TEMPLATE_FILE = os.environ["PWD"]+'/parking_email.jinja'
    template = templateEnv.get_template(TEMPLATE_FILE)

    templateVars = { "title" : "Test Example",
                     "description" : "A simple inquiry of function.",
                     "favorites" : newtickets
                   }

    outputText = template.render( templateVars )
    #Append new records to data to be written out to file
    for res in newtickets:
        print ("appending res")
        print (res)
        data.append(res)

    emailstring = baseemail + liststring
    emails = 'james.lapointe@gmail.com'


    emailutil.send_email(emails, 'Craigslist Parking Pass Check', outputText)
    with open(os.environ["PWD"]+'/my_file.json', 'w') as f:
        json.dump(data, f)
