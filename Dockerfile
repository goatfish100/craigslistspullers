FROM python:alpine3.6

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY parking_search.py ./
COPY emailutil.py ./
COPY parking_email.jinja ./


ENV GMAILUSER=james.lapointe@gmail.com
ENV GMAILPASSWORD=Codfish400!


RUN python parking_search.py